"""
Author: Paula Grangeiro <pgrangeiro.dev@gmail.com>

Generate Django's dynamic SECRET_KEY 
"""

import os

key_path = os.path.join(
		os.path.dirname(os.path.dirname(__file__)),
		'key.txt')
try:
	SECRET_KEY = open(key_path).read().strip()
except IOError:
	import random

	SECRET_KEY = ''.join([random.SystemRandom().choice(
		'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(_=+)')
		for i in range(50)])
	key_file = file(key_path, 'w')
	key_file.write(SECRET_KEY)
	key_file.close()